// Klasa WebBookmark dziedzicząca po klasie Bookmark
public class WebBookmark extends Bookmark {
    // Prywatne pole przechowujące informację o przeglądarce
    private String browser;

    // Konstruktor klasy
    public WebBookmark(String url, String title, String category, String browser) {
        // Wywołanie konstruktora klasy nadrzędnej (Bookmark) z podanymi parametrami
        super(url, title, category);
        // Inicjalizacja pola przeglądarki
        this.browser = browser;
    }

    // Metoda zwracająca informację o przeglądarce
    public String getBrowser() {
        return browser;
    }

    // Przesłonięta metoda toString zwracająca reprezentację tekstową obiektu
    @Override
    public String toString() {
        // Wywołanie metody toString klasy nadrzędnej oraz dodanie informacji o przeglądarce
        return super.toString() + ", Browser: " + browser;
    }
}
