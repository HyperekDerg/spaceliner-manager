import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

// Klasa BookmarkManager dziedzicząca po JFrame i implementująca interfejs Bookmarkable
public class BookmarkManager extends JFrame implements Bookmarkable {

    // Komponenty interfejsu użytkownika
    private JTextField urlField;
    private JTextField titleField;
    private JTextField categoryField;
    private JButton addButton;
    private JButton deleteButton;
    private JButton editButton;
    private JButton openButton;
    private JList<Bookmark> bookmarkList;
    private DefaultListModel<Bookmark> listModel;

    // Lista przechowująca zakładki
    private List<Bookmark> bookmarks;

    // Konstruktor klasy
    public BookmarkManager() {
        super("Bookmark Manager");
        bookmarks = new ArrayList<>();
        listModel = new DefaultListModel<>();
        bookmarkList = new JList<>(listModel);
        loadBookmarks();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 400);
        setLayout(new BorderLayout());

        // Ustawienie niestandardowego stylu
        setCustomLookAndFeel();

        JPanel inputPanel = new JPanel(new GridBagLayout());
        inputPanel.setBackground(new Color(240, 240, 240));
        inputPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(new Color(210, 210, 210)),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);

        gbc.gridx = 0;
        gbc.gridy = 0;
        inputPanel.add(new JLabel("URL:"), gbc);

        gbc.gridx = 1;
        urlField = new JTextField();
        inputPanel.add(urlField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        inputPanel.add(new JLabel("Title:"), gbc);

        gbc.gridx = 1;
        titleField = new JTextField();
        inputPanel.add(titleField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        inputPanel.add(new JLabel("Category:"), gbc);

        gbc.gridx = 1;
        categoryField = new JTextField();
        inputPanel.add(categoryField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        addButton = new JButton("Add Bookmark");
        styleButton(addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addBookmark();
            }
        });
        inputPanel.add(addButton, gbc);

        gbc.gridx = 1;
        deleteButton = new JButton("Delete Bookmark");
        styleButton(deleteButton);
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteBookmark();
            }
        });
        inputPanel.add(deleteButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        editButton = new JButton("Edit Bookmark");
        styleButton(editButton);
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editBookmark();
            }
        });
        inputPanel.add(editButton, gbc);

        gbc.gridx = 1;
        gbc.gridy = 4;
        openButton = new JButton("Open in Browser");
        styleButton(openButton);
        openButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openBookmark();
            }
        });
        inputPanel.add(openButton, gbc);

        add(inputPanel, BorderLayout.NORTH);

        bookmarkList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        bookmarkList.setBackground(new Color(250, 250, 250));
        bookmarkList.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(new Color(210, 210, 210)),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        JScrollPane scrollPane = new JScrollPane(bookmarkList);
        add(scrollPane, BorderLayout.CENTER);

        updateBookmarkList();
    }

    // Implementacja metod interfejsu Bookmarkable
    @Override
    public void addBookmark(String url, String title, String category) {
        Bookmark bookmark = new WebBookmark(url, title, category, "Default Browser");
        bookmarks.add(bookmark);
        updateBookmarkList();
        clearFields();
        saveBookmarks();
    }

    @Override
    public void deleteBookmark(String url) {
        Bookmark selectedBookmark = bookmarkList.getSelectedValue();
        if (selectedBookmark != null) {
            bookmarks.remove(selectedBookmark);
            updateBookmarkList();
            saveBookmarks();
        }
    }

    @Override
    public void editBookmark(String oldUrl, String newUrl, String newTitle, String newCategory) {
        for (Bookmark bookmark : bookmarks) {
            if (bookmark.getUrl().equals(oldUrl)) {
                bookmark.setUrl(newUrl);
                bookmark.setTitle(newTitle);
                bookmark.setCategory(newCategory);
                break;
            }
        }
        updateBookmarkList();
        saveBookmarks();
    }

    @Override
    public List<Bookmark> getAllBookmarks() {
        return bookmarks;
    }

    // Metoda dodająca zakładkę
    private void addBookmark() {
        String url = urlField.getText();
        String title = titleField.getText();
        String category = categoryField.getText();
        addBookmark(url, title, category);
    }

    // Metoda usuwająca zakładkę
    private void deleteBookmark() {
        Bookmark selectedBookmark = bookmarkList.getSelectedValue();
        if (selectedBookmark != null) {
            deleteBookmark(selectedBookmark.getUrl());
            clearFields();
        }
    }

    // Metoda edytująca zakładkę
    private void editBookmark() {
        Bookmark selectedBookmark = bookmarkList.getSelectedValue();
        if (selectedBookmark != null) {
            String newUrl = JOptionPane.showInputDialog(this, "Enter new URL:", selectedBookmark.getUrl());
            String newTitle = JOptionPane.showInputDialog(this, "Enter new title:", selectedBookmark.getTitle());
            String newCategory = JOptionPane.showInputDialog(this, "Enter new category:",
                    selectedBookmark.getCategory());
            if (newUrl != null && newTitle != null && newCategory != null) {
                editBookmark(selectedBookmark.getUrl(), newUrl, newTitle, newCategory);
                clearFields();
            }
        }
    }

    // Metoda otwierająca zakładkę w przeglądarce
    private void openBookmark() {
        Bookmark selectedBookmark = bookmarkList.getSelectedValue();
        if (selectedBookmark != null) {
            try {
                Desktop.getDesktop().browse(new URI(selectedBookmark.getUrl()));
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "Failed to open URL in browser", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    // Metoda aktualizująca listę zakładek
    private void updateBookmarkList() {
        listModel.clear();
        for (Bookmark bookmark : bookmarks) {
            listModel.addElement(bookmark);
        }
    }

    // Metoda czyszcząca pola wprowadzania danych
    private void clearFields() {
        urlField.setText("");
        titleField.setText("");
        categoryField.setText("");
    }

    // Metoda zapisująca zakładki do pliku
    private void saveBookmarks() {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream("bookmarks.ser"))) {
            outputStream.writeObject(bookmarks);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Metoda wczytująca zakładki z pliku
    @SuppressWarnings("unchecked")
    private void loadBookmarks() {
        try {
            File file = new File("bookmarks.ser");
            if (!file.exists()) {
                file.createNewFile();
                bookmarks = new ArrayList<>();
                return;
            }

            try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file))) {
                Object obj = inputStream.readObject();
                if (obj instanceof List<?>) {
                    bookmarks = (List<Bookmark>) obj;
                    updateBookmarkList();
                } else {
                    throw new InvalidDataFormatException("Invalid data format in bookmarks.ser");
                }
            }
        } catch (IOException | ClassNotFoundException | InvalidDataFormatException e) {
            e.printStackTrace();
            bookmarks = new ArrayList<>();
        }
    }

    // Metoda ustawiająca niestandardowy wygląd interfejsu
    private void setCustomLookAndFeel() {
        UIManager.put("Button.background", new Color(70, 130, 180));
        UIManager.put("Button.foreground", Color.WHITE);
        UIManager.put("Button.focus", Color.LIGHT_GRAY);
        UIManager.put("Button.select", new Color(70, 130, 180));
        UIManager.put("Button.border", BorderFactory.createLineBorder(new Color(70, 130, 180)));

        UIManager.put("TextField.border", BorderFactory.createLineBorder(new Color(210, 210, 210)));

        UIManager.put("List.background", new Color(250, 250, 250));
        UIManager.put("List.border", BorderFactory.createLineBorder(new Color(210, 210, 210)));

        UIManager.put("ScrollPane.border", BorderFactory.createLineBorder(new Color(210, 210, 210)));
    }

    // Metoda ustawiająca niestandardowe właściwości przycisku
    private void styleButton(JButton button) {
        button.setFocusPainted(false);
        button.setBorder(BorderFactory.createLineBorder(new Color(70, 130, 180)));
        button.setForeground(Color.WHITE);
        button.setBackground(new Color(70, 130, 180));
    }

    // Metoda główna
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    // Ustawienie niestandardowego wyglądu
                    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                } catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException
                        | IllegalAccessException e) {
                    e.printStackTrace();
                }
                new BookmarkManager().setVisible(true);
            }
        });
    }
}