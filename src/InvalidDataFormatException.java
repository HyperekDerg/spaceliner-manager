// Wyjątek InvalidDataFormatException reprezentujący sytuację niepoprawnego formatu danych
public class InvalidDataFormatException extends Exception {
    // Konstruktor klasy, przyjmujący komunikat o błędzie
    public InvalidDataFormatException(String message) {
        // Wywołanie konstruktora klasy nadrzędnej (Exception) z podanym komunikatem
        super(message);
    }
}
