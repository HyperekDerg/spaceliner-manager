import java.util.List;

// Interfejs Bookmarkable definiujący operacje na zakładkach
public interface Bookmarkable {
    // Metoda dodająca nową zakładkę
    void addBookmark(String url, String title, String category);
    
    // Metoda usuwająca zakładkę na podstawie URL
    void deleteBookmark(String url);
    
    // Metoda edytująca istniejącą zakładkę
    void editBookmark(String oldUrl, String newUrl, String newTitle, String newCategory);
    
    // Metoda zwracająca listę wszystkich zakładek
    List<Bookmark> getAllBookmarks();
}
