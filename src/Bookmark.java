import java.io.Serializable;

// Klasa abstrakcyjna Bookmark implementująca interfejs Serializable
public abstract class Bookmark implements Serializable {
    // Prywatne pola klasy
    private String url; // URL zakładki
    private String title; // Tytuł zakładki
    private String category; // Kategoria zakładki

    // Konstruktor klasy Bookmark
    public Bookmark(String url, String title, String category) {
        this.url = url;
        this.title = title;
        this.category = category;
    }

    // Metoda zwracająca URL zakładki
    public String getUrl() {
        return url;
    }

    // Metoda ustawiająca URL zakładki
    public void setUrl(String url) {
        this.url = url;
    }

    // Metoda zwracająca tytuł zakładki
    public String getTitle() {
        return title;
    }

    // Metoda ustawiająca tytuł zakładki
    public void setTitle(String title) {
        this.title = title;
    }

    // Metoda zwracająca kategorię zakładki
    public String getCategory() {
        return category;
    }

    // Metoda ustawiająca kategorię zakładki
    public void setCategory(String category) {
        this.category = category;
    }

    // Przesłonięta metoda toString zwracająca reprezentację tekstową obiektu
    @Override
    public String toString() {
        return "Title: " + title + ", URL: " + url + ", Category: " + category;
    }
}
